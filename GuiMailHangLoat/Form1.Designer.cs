﻿
namespace GuiMailHangLoat
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Đến = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtReceive = new System.Windows.Forms.TextBox();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnAttach = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.txtFileAttach = new System.Windows.Forms.TextBox();
            this.btnListAttach = new System.Windows.Forms.Button();
            this.lstReceive = new System.Windows.Forms.ListBox();
            this.btnDelSelected = new System.Windows.Forms.Button();
            this.boxSave = new System.Windows.Forms.CheckBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.boxServers = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // Đến
            // 
            this.Đến.AutoSize = true;
            this.Đến.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.Đến.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Đến.Location = new System.Drawing.Point(14, 300);
            this.Đến.Name = "Đến";
            this.Đến.Size = new System.Drawing.Size(61, 33);
            this.Đến.TabIndex = 2;
            this.Đến.Text = "Đến";
            this.Đến.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(12, 446);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 33);
            this.label4.TabIndex = 3;
            this.label4.Text = "Chủ đề";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label5.Location = new System.Drawing.Point(19, 529);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 33);
            this.label5.TabIndex = 4;
            this.label5.Text = "Nội dung";
            // 
            // txtUserName
            // 
            this.txtUserName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUserName.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtUserName.ForeColor = System.Drawing.Color.Orange;
            this.txtUserName.Location = new System.Drawing.Point(102, 96);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(405, 38);
            this.txtUserName.TabIndex = 5;
            this.txtUserName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPassword
            // 
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPassword.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtPassword.ForeColor = System.Drawing.Color.Orange;
            this.txtPassword.Location = new System.Drawing.Point(102, 184);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(405, 38);
            this.txtPassword.TabIndex = 6;
            this.txtPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPassword.TextChanged += new System.EventHandler(this.txtPassword_TextChanged);
            // 
            // txtReceive
            // 
            this.txtReceive.BackColor = System.Drawing.Color.Gainsboro;
            this.txtReceive.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtReceive.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtReceive.Location = new System.Drawing.Point(119, 290);
            this.txtReceive.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtReceive.Name = "txtReceive";
            this.txtReceive.Size = new System.Drawing.Size(552, 38);
            this.txtReceive.TabIndex = 7;
            this.txtReceive.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSubject
            // 
            this.txtSubject.BackColor = System.Drawing.Color.White;
            this.txtSubject.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSubject.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtSubject.Location = new System.Drawing.Point(119, 436);
            this.txtSubject.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(552, 38);
            this.txtSubject.TabIndex = 8;
            this.txtSubject.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtMessage
            // 
            this.txtMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMessage.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtMessage.Location = new System.Drawing.Point(22, 566);
            this.txtMessage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(708, 308);
            this.txtMessage.TabIndex = 9;
            // 
            // btnSend
            // 
            this.btnSend.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnSend.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSend.FlatAppearance.BorderSize = 0;
            this.btnSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSend.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSend.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSend.Location = new System.Drawing.Point(25, 906);
            this.btnSend.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(154, 54);
            this.btnSend.TabIndex = 10;
            this.btnSend.Text = "Gửi";
            this.btnSend.UseVisualStyleBackColor = false;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnAttach
            // 
            this.btnAttach.BackColor = System.Drawing.Color.Transparent;
            this.btnAttach.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAttach.BackgroundImage")));
            this.btnAttach.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAttach.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAttach.FlatAppearance.BorderSize = 0;
            this.btnAttach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAttach.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnAttach.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAttach.Location = new System.Drawing.Point(207, 916);
            this.btnAttach.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAttach.Name = "btnAttach";
            this.btnAttach.Size = new System.Drawing.Size(40, 40);
            this.btnAttach.TabIndex = 11;
            this.btnAttach.UseVisualStyleBackColor = false;
            this.btnAttach.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.MidnightBlue;
            this.panel1.Location = new System.Drawing.Point(19, 338);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(721, 2);
            this.panel1.TabIndex = 12;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MidnightBlue;
            this.panel2.Location = new System.Drawing.Point(18, 484);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(721, 2);
            this.panel2.TabIndex = 13;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.MidnightBlue;
            this.panel3.Location = new System.Drawing.Point(36, 148);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(471, 2);
            this.panel3.TabIndex = 15;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.MidnightBlue;
            this.panel4.Location = new System.Drawing.Point(33, 231);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(475, 2);
            this.panel4.TabIndex = 16;
            // 
            // button3
            // 
            this.button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button3.BackgroundImage")));
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(36, 75);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(46, 65);
            this.button3.TabIndex = 17;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.White;
            this.button4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button4.BackgroundImage")));
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(36, 168);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(46, 56);
            this.button4.TabIndex = 18;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // txtFileAttach
            // 
            this.txtFileAttach.BackColor = System.Drawing.Color.LightGray;
            this.txtFileAttach.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFileAttach.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtFileAttach.Location = new System.Drawing.Point(254, 919);
            this.txtFileAttach.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFileAttach.Name = "txtFileAttach";
            this.txtFileAttach.Size = new System.Drawing.Size(477, 35);
            this.txtFileAttach.TabIndex = 19;
            this.txtFileAttach.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtFileAttach.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // btnListAttach
            // 
            this.btnListAttach.BackColor = System.Drawing.Color.Transparent;
            this.btnListAttach.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnListAttach.BackgroundImage")));
            this.btnListAttach.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnListAttach.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnListAttach.FlatAppearance.BorderSize = 0;
            this.btnListAttach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListAttach.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnListAttach.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnListAttach.Location = new System.Drawing.Point(688, 272);
            this.btnListAttach.Margin = new System.Windows.Forms.Padding(0);
            this.btnListAttach.Name = "btnListAttach";
            this.btnListAttach.Size = new System.Drawing.Size(43, 59);
            this.btnListAttach.TabIndex = 20;
            this.btnListAttach.UseVisualStyleBackColor = false;
            this.btnListAttach.Click += new System.EventHandler(this.btnListAttach_Click);
            // 
            // lstReceive
            // 
            this.lstReceive.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lstReceive.FormattingEnabled = true;
            this.lstReceive.ItemHeight = 32;
            this.lstReceive.Location = new System.Drawing.Point(767, 131);
            this.lstReceive.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lstReceive.Name = "lstReceive";
            this.lstReceive.Size = new System.Drawing.Size(421, 676);
            this.lstReceive.TabIndex = 21;
            // 
            // btnDelSelected
            // 
            this.btnDelSelected.BackColor = System.Drawing.Color.Crimson;
            this.btnDelSelected.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelSelected.FlatAppearance.BorderSize = 0;
            this.btnDelSelected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelSelected.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelSelected.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDelSelected.Location = new System.Drawing.Point(1035, 74);
            this.btnDelSelected.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDelSelected.Name = "btnDelSelected";
            this.btnDelSelected.Size = new System.Drawing.Size(154, 54);
            this.btnDelSelected.TabIndex = 22;
            this.btnDelSelected.Text = "Xóa";
            this.btnDelSelected.UseVisualStyleBackColor = false;
            this.btnDelSelected.Click += new System.EventHandler(this.btnDelSelected_Click);
            // 
            // boxSave
            // 
            this.boxSave.AutoSize = true;
            this.boxSave.BackColor = System.Drawing.Color.Transparent;
            this.boxSave.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.boxSave.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.boxSave.Location = new System.Drawing.Point(514, 195);
            this.boxSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.boxSave.Name = "boxSave";
            this.boxSave.Size = new System.Drawing.Size(86, 37);
            this.boxSave.TabIndex = 23;
            this.boxSave.Text = "Lưu";
            this.boxSave.UseVisualStyleBackColor = false;
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            ".xlsx",
            ".txt"});
            this.comboBox1.Location = new System.Drawing.Point(650, 348);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(88, 37);
            this.comboBox1.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(583, 355);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 33);
            this.label1.TabIndex = 25;
            this.label1.Text = "type";
            // 
            // boxServers
            // 
            this.boxServers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.boxServers.FormattingEnabled = true;
            this.boxServers.Items.AddRange(new object[] {
            ".Gmail",
            ".Yahoo"});
            this.boxServers.Location = new System.Drawing.Point(588, 15);
            this.boxServers.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.boxServers.Name = "boxServers";
            this.boxServers.Size = new System.Drawing.Size(155, 37);
            this.boxServers.TabIndex = 26;
            this.boxServers.Text = ".Gmail";
            this.boxServers.SelectedIndexChanged += new System.EventHandler(this.boxServers_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(753, 1050);
            this.Controls.Add(this.boxServers);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.boxSave);
            this.Controls.Add(this.btnDelSelected);
            this.Controls.Add(this.lstReceive);
            this.Controls.Add(this.btnListAttach);
            this.Controls.Add(this.txtFileAttach);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnAttach);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.txtMessage);
            this.Controls.Add(this.txtSubject);
            this.Controls.Add(this.txtReceive);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Đến);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(775, 1111);
            this.MinimumSize = new System.Drawing.Size(775, 1018);
            this.Name = "Form1";
            this.Text = "Gửi Mail";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtReceive;
        private System.Windows.Forms.TextBox txtSubject;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnAttach;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label Đến;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtFileAttach;
        private System.Windows.Forms.Button btnListAttach;
        private System.Windows.Forms.ListBox lstReceive;
        private System.Windows.Forms.Button btnDelSelected;
        private System.Windows.Forms.CheckBox boxSave;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox boxServers;
    }
}

