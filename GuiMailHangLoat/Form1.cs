﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Threading;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using Excel = Microsoft.Office.Interop.Excel; 

namespace GuiMailHangLoat
{
    public partial class Form1 : Form
    {
       
        public Form1()
        {
            InitializeComponent();
        }
        Attachment attach = null;

        private void Form1_Load_1(object sender, EventArgs e)
        { 
            ///Load lại tên đăng nhập đã lưu
            txtUserName.Text = Properties.Settings.Default.username;
            txtPassword.Text = Properties.Settings.Default.password;
        }


        /// <summary>
        /// chức năng gửi mail
        /// </summary>
        /// <param name="from">người gửi</param>
        /// <param name="to">người nhận</param>
        /// <param name="subject">tiêu đề</param>
        /// <param name="message">nội dung</param>
        /// <param name="file">đính kèm (nếu có)</param>
        void sendEmail(string from, string to, string subject, string message, Attachment file = null)
        {
            MailMessage mess = new MailMessage(from, to, subject, message);
            
            //file đính kèm
            if(attach != null)
            {
                mess.Attachments.Add(attach);
            }
            //smtp server quyết định người gửi sử dụng server mail nào (gmail/yahoo)
            //if (boxServers.Text == ".Gmail")
           // {
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(txtUserName.Text, txtPassword.Text);
                client.Send(mess);
            //}
           // if(boxServers.Text == ".Yahoo")
           // {
            //    SmtpClient client = new SmtpClient("smtp.mail.yahoo.com", 465);
             //   client.EnableSsl = true;
             //   client.Credentials = new NetworkCredential(txtUserName.Text, txtPassword.Text);
             //   client.Send(mess);
           // }
            
        }
        
        //Xóa
        void clearMessBox()
        {
            this.Invoke((MethodInvoker)delegate
            {
                txtSubject.Clear();
                txtMessage.Clear();
                txtFileAttach.Clear();
            });
        }

        //attach button file đính kèm
        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if(dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFileAttach.Text = dialog.FileName;
            }
        }

        //send button
        private void btnSend_Click(object sender, EventArgs e)
        {
            if (checkAll())
            {
                Thread thread = new Thread(() =>
                {
                    attach = null;
                    try
                    {
                        FileInfo file = new FileInfo(txtFileAttach.Text);
                        attach = new Attachment(txtFileAttach.Text);
                    }
                    catch { }

                    Regex reg = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                    try
                    {
                        if (!reg.IsMatch(txtReceive.Text) || lstReceive.Items.Count != 0)///nếu tồn tại list email
                        {
                            for (int i = 0; i < lstReceive.Items.Count; i++)
                            {
                                string email = lstReceive.Items[i].ToString();
                                if (!string.IsNullOrWhiteSpace(email))
                                {
                                    if (!reg.IsMatch(email))
                                    {
                                        MessageBox.Show("Email " + email + " invalid!");
                                    }
                                    else
                                    {
                                        sendEmail(txtUserName.Text, email, txtSubject.Text, txtMessage.Text);
                                    }
                                }
                            }
                        }
                        else if(!reg.IsMatch(txtReceive.Text))///Gửi mail cá nhân
                            {
                                MessageBox.Show("Email: " + txtReceive.Text + " invalid");
                            }
                         else
                         {
                              this.Invoke((MethodInvoker)delegate
                              {
                                   comboBox1.Text = "";
                               });
                               sendEmail(txtUserName.Text, txtReceive.Text, txtSubject.Text, txtMessage.Text);
                          }
                        

                        if (boxSave.Checked == true)//Save acount
                        {
                            Properties.Settings.Default.username = txtUserName.Text;
                            Properties.Settings.Default.password = txtPassword.Text;
                            Properties.Settings.Default.Save();
                        }
                        else
                        {
                            Properties.Settings.Default.username = "Tên đăng nhập";
                            Properties.Settings.Default.password = "Mật khẩu";
                            Properties.Settings.Default.Save();
                        }
                        MessageBox.Show("Đã gửi");
                        clearMessBox();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex.ToString());
                    }
                });
                thread.Start();
            }
            else
            {
                MessageBox.Show("Thiếu dữ liệu");
            }
            
        }

        /// <summary>
        /// Thêm danh sách email tùy loại file .txt/excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnListAttach_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "")
            {
                MessageBox.Show("Chọn type");
            }
            else
            {
                txtReceive.Clear();
                lstReceive.Items.Clear();
                this.MinimumSize = new Size(900, 0);
                this.MaximumSize = new Size(900, 900);
                OpenFileDialog dialog = new OpenFileDialog();
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    txtReceive.Text = dialog.FileName;
                }

                //mở file .txt
                if (comboBox1.Text == ".txt")
                {
                    StreamReader sr = new StreamReader(txtReceive.Text);
                    string email;
                    while ((email = sr.ReadLine()) != null)
                    {
                        lstReceive.Items.Add(email);
                    }
                    sr.Close();
                }
                else if (comboBox1.Text == ".xlsx")//mở file .xlsx
                {
                    string FName = txtReceive.Text;
                    var excelApp = new Excel.Application();
                    excelApp.Visible = true;

                    Excel.Workbook excelbk = excelApp.Workbooks._Open(FName,
                       Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                       Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                       Type.Missing, Type.Missing);

                    Excel.Worksheet xlSht = (Excel.Worksheet)excelbk.Worksheets["Sheet1"];

                    //find Column Number
                    //Now find Test in Row 1
                    Excel.Range Row1 = (Excel.Range)xlSht.Rows[1, Type.Missing];
                    string FindWhat = "email";
                    bool MatchCase = true;

                    Excel.Range FindResults = Row1.Find(FindWhat, Type.Missing,
                        Excel.XlFindLookIn.xlValues, Excel.XlLookAt.xlWhole,
                        Excel.XlSearchOrder.xlByColumns, Excel.XlSearchDirection.xlNext,
                        MatchCase, Type.Missing, Type.Missing);


                    int colNumber = FindResults.Column;

                    //Get Last Row of Data
                    Excel.Range xlRange = (Excel.Range)xlSht.get_Range("A" + xlSht.Rows.Count, Type.Missing);
                    int LastRow = xlRange.get_End(Microsoft.Office.Interop.Excel.XlDirection.xlUp).Row;

                    //start update
                    lstReceive.BeginUpdate();

                    //read data into form
                    string CellData;
                    for (int RowCount = 2; RowCount <= LastRow; RowCount++)
                    {
                        xlRange = (Excel.Range)xlSht.Cells[RowCount, colNumber];
                        CellData = (string)xlRange.Text;
                        lstReceive.Items.Add(CellData);

                    }

                    //end update
                    lstReceive.EndUpdate();

                    object SaveChanges = (object)false;
                    excelbk.Close(SaveChanges, Type.Missing, Type.Missing);
                    excelApp.Quit();
                    excelApp = null;
                }

            }


        }

        /// <summary>
        /// Xóa item trong list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelSelected_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < lstReceive.SelectedItems.Count; i++)
            {
                lstReceive.Items.Remove(lstReceive.SelectedItems[i]);
            }
        }

        public void boxSave_CheckedChanged(object sender, EventArgs e)//Check checkBox lưu mật khẩu
        {
            if (txtPassword.Text != "" && txtUserName.Text != "")
            {
                if (boxSave.Checked == true)
                {
                    MessageBox.Show(Properties.Settings.Default.username);
                    string user = txtUserName.Text;
                    string password = txtPassword.Text;
                    Properties.Settings.Default.username = user;
                    Properties.Settings.Default.password = password;
                    Properties.Settings.Default.Save();
                }
                else
                {
                    Properties.Settings.Default.Reset();
                }
            }
        }

        public bool checkAll()
        {
            if (string.IsNullOrEmpty(txtUserName.Text) || string.IsNullOrEmpty(txtPassword.Text) || string.IsNullOrEmpty(txtMessage.Text)
                || string.IsNullOrEmpty(txtReceive.Text) || boxServers.Text == "") 
            {
                return false;
            }
            return true;
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void boxServers_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
